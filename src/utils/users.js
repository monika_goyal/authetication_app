const registeredUsers = [
    { userName: 'john', token: 'qwerty', role: 'admin'},
    { userName: 'jane', token: 'abcd', role: 'user'},
    { userName: 'jack', token: 'efgh', role: 'user'}
]

export const checkIfUserExists = user => {
    const existingUser = registeredUsers.find(registeredUsers => {
        return registeredUsers.userName === user.userName && registeredUsers.token === user.token && registeredUsers.role === user.role
    })
    return existingUser
}