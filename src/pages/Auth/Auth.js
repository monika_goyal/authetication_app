import Login from "./Login/Login"

const Auth = props => {

    const goToHomePage = () => {
        props.history.push('/home')
    }

    return (
        <div>
            <Login
                goToHomePage={goToHomePage}
                setAuthorizedState={props.setAuthorizedState}
                setUser={props.setUser}
            />
        </div>
    )
}

export default Auth