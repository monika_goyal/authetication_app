import { Button, MenuItem, TextField } from "@material-ui/core"
import { useState } from "react"
import classes from '../Auth.module.css'
import { checkIfUserExists } from '../../../utils/users'
import { setUserInStorage } from "../../../services/storage-services"
import { loginUser } from '../../../services/login-services'

const INITIAL_STATE = {
    userName: '',
    password: '',
    role: 'user'
}

const Login = props => {
    const [ userData, setUserData ] = useState(INITIAL_STATE)
    const [loginErrMsg, setLoginErrMsg] = useState('')

    const onChangeHandler = e => {
        setUserData({ ...userData, [e.target.name]: e.target.value})
    }

    const onLogin = async (e) => {
        e.preventDefault()
        const { userName, password, role } = userData
        if(!userName || !password){
            return
        }
        const user = { userName, token: password, role}
            console.log(user)
            await loginUser(user)
            const response = checkIfUserExists(user)
            console.log(response)
            if(response){
                setUserInStorage(user)
                props.goToHomePage()
                props.setAuthorizedState(true)
                props.setUser(response)
            }
            else{
                setLoginErrMsg('Unauthorized Access')
                setUserData(INITIAL_STATE)
            }
        
    }

    return (
        <div>
            <h3 style={{ textAlign: "center" }}>Login Form</h3>
            <form className={classes["login-form"]} onSubmit={onLogin}>
                <TextField
                    variant="outlined"
                    label="User Name"
                    type="text"
                    margin="normal"
                    name="userName"
                    value={userData.userName}
                    onChange={onChangeHandler}
                />
                <TextField
                    variant="outlined"
                    label="Password"
                    type="text"
                    margin="normal"
                    name="password"
                    value={userData.password}
                    onChange={onChangeHandler}
                />
                <TextField
                    select
                    variant="outlined"
                    label="Role"
                    type="text"
                    margin="normal"
                    name="role"
                    value={userData.role}
                    onChange={onChangeHandler}
                >
                    <MenuItem value="admin">Administrator</MenuItem>
                    <MenuItem value="user">User</MenuItem>
                </TextField>
                {loginErrMsg ? <small style={{color: "red"}}>{loginErrMsg}</small> : null}
                <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                >
                    Submit
                </Button>
            </form>
        </div>
    )
}

export default Login