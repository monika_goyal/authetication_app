import { Switch, Redirect, Route } from "react-router-dom"
import Auth from "../pages/Auth/Auth"
import Home from '../pages/Home/Home'
import PageNotFound from '../pages/PageNotFound/PageNotFound'

const Routes = props => {
    const setAuthorizedState = props.setAuthorizedState
    const setUser = props.setUser
    return (
        <Switch>
            <Redirect from="/" to="/home" exact />
            <Route path="/home" component={Home} />
            <Route path="/auth" render={props => <Auth {...props} setAuthorizedState={setAuthorizedState} setUser={setUser} /> } />
            <Route path="*" component={PageNotFound} />
        </Switch>
    )
}

export default Routes