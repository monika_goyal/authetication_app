import { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './App.css';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Routes from './routes/Routes';
import { getUserFromStorage } from './services/storage-services'

class App extends Component {
	state = {
		authorized: false,
		user: null
	}

	componentDidMount(){
		const user = getUserFromStorage()
		if(user){
			this.setState({ authorized: true, user: user })
		}
		else{
			this.props.history.push('/auth')
		}
	}

	setAuthorizedState = authorized => {
		this.setState({ authorized })
	}

	setUser = user => {
		this.setState({ user })
	}

	render(){
		return (
			<>
				<div className="container">
					<Header 
						authorized={this.state.authorized} 
						setAuthorizedState={this.setAuthorizedState}
						user={this.state.user}
					/>
					<Routes 
						setAuthorizedState={this.setAuthorizedState} 
						setUser={this.setUser}
					/>
					<Footer />
				</div>
			</>
		)
	}
}

export default withRouter(App);
