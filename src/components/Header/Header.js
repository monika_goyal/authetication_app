import { AppBar, Button, Toolbar, Typography } from "@material-ui/core"
import { withRouter } from "react-router-dom"
import { clearUserFromStorage } from "../../services/storage-services"
import HeaderLink from './HeaderLink'

const Header = props => {

    const onLogout = () => {
        clearUserFromStorage()
        props.setAuthorizedState(false)
        props.history.push('/auth')
    }
    
    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" component="div"> Logo </Typography>
                { props.authorized ? <>
                    { props.user ? props.user.role === 'user' ? <HeaderLink path="Home" />
                        :  
                        <>
                            <HeaderLink path="Home" />
                            <HeaderLink path="Conditional" />
                            <HeaderLink path="Posts" />
                        </>
                        : null
                    }
                        
                        <Button
                            variant="outlined"
                            color="inherit"
                            style={{position: "absolute", right: "20px"}}
                            onClick={onLogout}
                        >
                            Logout
                        </Button></>
                    : 
                    null    
                }
                
            </Toolbar>
        </AppBar>
    )
}

export default withRouter(Header)