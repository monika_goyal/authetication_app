import { NavLink } from "react-router-dom"
import classes from './Header.module.css'

const HeaderLink = props => {
    return (
        <NavLink activeClassName={classes.active} to={`/${props.path.toLowerCase()}`}>
            <span style={{cursor: "pointer", color: "#fff", marginLeft: "10px"}}>{props.path}</span>
        </NavLink>
    )
}

export default HeaderLink