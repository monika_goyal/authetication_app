export const setUserInStorage = user => {
    sessionStorage.setItem('user', JSON.stringify(user))
}

export const getUserFromStorage = () => {
    return JSON.parse(sessionStorage.getItem('user'))
}

export const clearUserFromStorage = () => {
    sessionStorage.clear()
}